var Note = require('../models/note');
var History = require('../controllers/historynote');

//Create Note Easy
exports.create = async function (req, h) {
    var newNote = new Note();
    var status = {};
    newNote.notename = req.payload.notename;
    newNote.detailnote = req.payload.detailnote;
    newNote.categorynote = req.payload.categorynote;
    newNote.customernote = req.payload.customernote;
    newNote.createdate = Date.now();
    newNote.modifydate = Date.now();

    await newNote.save().then((docs)=>{
        status = docs;
        History.create(newNote._id, newNote.categorynote, newNote.customernote);
    })

    return {
        message:"++++Add Success++++"
    }
}