const mongoose = require('mongoose');

const nodeShema = new mongoose.Schema({
    notename: { type: String },
    detailnote: { type: String },
    categorynote: { type: String },
    customernote: { type: String },
    createdate: { type: Date },
    modifydate: { type: Date },
})

module.exports = mongoose.model('note', nodeShema);