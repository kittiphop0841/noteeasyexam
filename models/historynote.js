const mongoose = require('mongoose');

const historynoteShema = new mongoose.Schema({
    noteid:{type:Object},
    categorynotename:{type:Object},
    customername:{type:Object},
    createdate:{type:Date},
    modifydate:{type:Date},
})

module.exports = mongoose.model('historynote', historynoteShema);