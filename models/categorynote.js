const mongoose = require('mongoose');

const categorynoteShema = new mongoose.Schema({
    // Category Note Name [All, Blog, To-Do]
    categorynotename: { type: String },
    // Category Code [NOTE01, NOTE02]
    categorycode: { type: String },
    // Create Date Note
    createdate: { type: Date },
    // Modify Date Note
    modifydate: { type: Date },
})

module.exports = mongoose.model('categorynote', categorynoteShema);