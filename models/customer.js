const mongoose = require('mongoose');

const customerShema = new mongoose.Schema({
    customername:{type:String},
    createdate:{type:Date},
    modifydate:{type:Date},
})

module.exports = mongoose.model('customer', customerShema);