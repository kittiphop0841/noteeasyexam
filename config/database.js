const mongoose = require('mongoose');

exports.db = ()=>{
    mongoose.connect("mongodb://localhost:27017/NoteEasy", {
        useNewUrlParser:true,
        useUnifiedTopology:true
    })
    .then(()=>{
        console.log('Successfully connected to database');
    })
    .catch((err)=>{
        console.log('Error Connecting to database');
        console.log(err);
        process.exit(1);
    })
}