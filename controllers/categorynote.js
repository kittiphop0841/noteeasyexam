var Categorynote = require('../models/categorynote');

exports.create = async function (req, res) {

    var newCategory = new Categorynote();
    var CategoryData = {};
    newCategory.categorynotename = req.payload.categorynotename;
    newCategory.categorycode = req.payload.categorycode;
    newCategory.createdate = Date.now();
    newCategory.modifydate = Date.now();

    await newCategory.save().then((docs) => {
        CategoryData = docs;
    })

    return CategoryData._doc;
}

exports.search = function (req, res) {
    var data = Categorynote.find({})
    
    return data;
}