import { createRouter, createWebHistory } from 'vue-router'
import HistoryNote from '../view/HistoryNote.vue'
import CreateNote from '../view/CreateNote.vue'
import CustomerNote from '../view/CustomerNote.vue'

const routes = [
    {
        path: '/',
        component: HistoryNote
    },
    {
        path: '/add',
        component:CreateNote
    },
    {
        path: '/customer',
        component:CustomerNote
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URl),
    routes
})

export default router