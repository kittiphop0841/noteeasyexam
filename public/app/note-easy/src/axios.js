import axios from 'axios';

export const HTTP = axios.create({
    baseURL: 'http://localhost:8081/',
    timeout: 10000,
    withCredentials: true,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
});