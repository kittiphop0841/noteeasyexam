var Historynote = require('../models/historynote');


exports.create = function (noteid, categorynotename, customername) {
    var newloghistory = new Historynote();

    if (noteid) {
        newloghistory.noteid = noteid;
    }

    newloghistory.categorynotename = categorynotename;
    newloghistory.customername = customername;
    newloghistory.createdate = Date.now();
    newloghistory.modifydate = Date.now();

    newloghistory.save()
    
}

exports.search = function (req, res) {
  var data = Historynote.aggregate([
    {
      $lookup:
      {
        from: "notes",
        localField: "noteid",
        foreignField: "_id",
        as: "noteid"
      }
    },
    {
      $unwind:
      {
        path: "$noteid",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $project:
      {
        __v: 0,
        categorynotename: 0,
        customername:0
      }
    },
  ])
    return data;
}