var Customer = require('../models/customer');

exports.create = async function (req, h) {
    var newCustomer = new Customer();
    var CustomerData = {};
    newCustomer.customername = req.payload.customername;
    newCustomer.createdate = Date.now();
    newCustomer.modifydate = Date.now();

    await newCustomer.save().then((docs)=>{
        CustomerData=docs;
    })

    return {
        message:"++++Add Success++++"
    }
}

exports.search = function (req, res) {
    var dataCustomer = Customer.find({})
    return dataCustomer;
}