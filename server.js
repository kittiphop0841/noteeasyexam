'use strict';

const Hapi = require('@hapi/hapi');
const PORT = process.env.PORT || 8081;
var Note = require('./controllers/note');
var CategoryNote = require('./controllers/categorynote');
var CustomerNote = require('./controllers/customer');
var HistoryNote = require('./controllers/historynote');

require("./config/database").db();

const init = async () => {

    const server = Hapi.server({
        port: PORT,
        host: 'localhost',
        routes: {
            cors: {
                origin: ['*'],
                headers: ['Authorization','Accept', 'Content-Type'],
                exposedHeaders: ['Accept'],
                additionalExposedHeaders: ['Accept'],
                additionalHeaders: ["X-Requested-With"],
                maxAge: 60,
                credentials: true
            }
        }
    });

    server.route({
        method: 'POST',
        path: '/api/note/createnote',
        handler: Note.create
    });

    server.route({
        method: 'POST',
        path: '/api/categorynote/create',
        handler: CategoryNote.create
    });

    server.route({
        method: 'GET',
        path: '/api/categorynote/search',
        handler: CategoryNote.search
    });

    server.route({
        method: 'POST',
        path: '/api/customernote/create',
        handler: CustomerNote.create
    });

    server.route({
        method: 'GET',
        path: '/api/customernote/search',
        handler: CustomerNote.search
    });

    server.route({
        method: 'GET',
        path: '/api/historynote/search',
        handler: HistoryNote.search
    });

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

init();
